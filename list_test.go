package icontainer

import (
	"fmt"
	"testing"
)

func ExampleList() {
	type node struct {
		ListNode[*node]
		Value string
	}
	ls := NewList(
		&node{Value: "foo"},
		&node{Value: "bar"},
		&node{Value: "baz"},
	)
	for n := ls.Front(); n != nil; n = n.IterNext() {
		fmt.Println(n.Value)
	}
	for n := ls.Back(); n != nil; n = n.IterPrev() {
		fmt.Println(n.Value)
	}
	// Output:
	// foo
	// bar
	// baz
	// baz
	// bar
	// foo
}

type testListNode struct {
	ListNode[*testListNode]
	val string
}

func checkListLinks(t *testing.T, ls *List[*testListNode], es ...*testListNode) (ok bool) {
	l := len(es)
	if l == 0 {
		ok = true
		if ls.length != 0 {
			t.Errorf("have len %d of empty lits", ls.length)
			ok = false
		}
		if ls.front != nil {
			t.Error("front of empty list is not nil")
			ok = false
		}
		if ls.back != nil {
			t.Errorf("front of empty list is not nil")
			ok = false
		}
		return ok
	}
	ok = true
	if tmp := ls.Len(); tmp != l {
		t.Errorf("unexpected len %d, want %d", tmp, l)
	}
	if ls.front != es[0] {
		t.Errorf("front points to %p (%s), expect %p (%s)",
			ls.front, ls.front.val,
			&es[0], es[0].val,
		)
		ok = false
	}
	if ls.back != es[l-1] {
		t.Errorf("back points to %p (%s), expect %p (%s)",
			ls.back, ls.back.val,
			&es[l-1], es[l-1].val,
		)
	}
	for ei, le := 0, ls.Front(); ei < l; ei, le = ei+1, le.next {
		if le == nil {
			t.Fatalf("no list element %d, expect %p (%s)", ei, es[ei], es[ei].val)
		}
		if le != es[ei] {
			t.Fatalf("unexpected element %d: %p (%s), expect %p (%s)",
				ei,
				le, le.val,
				es[ei], es[ei].val,
			)
		}
		if ei == 0 && le.prev != nil {
			t.Errorf("front %p (%s) has prev %p (%s)",
				le, le.val,
				le.prev, le.prev.val,
			)
			ok = false
		}
		if ei == l-1 && le.next != nil {
			t.Errorf("back %p (%s) has next %p (%s)",
				le, le.val,
				le.next, le.next.val,
			)
			ok = false
		}
	}
	return ok
}

func TestList_New(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	t.Run("empty", func(t *testing.T) {
		ls := NewList[*testListNode]()
		checkListLinks(t, ls)
	})
	for i := 1; i <= len(es); i++ {
		t.Run(fmt.Sprintf("%d elements", i), func(t *testing.T) {
			ls := NewList[*testListNode](es[:i]...)
			checkListLinks(t, ls, es[:i]...)
		})
	}
}

func TestList_PushBack(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	ls := NewList[*testListNode]()
	for i := 0; i < len(es); i++ {
		ls.PushBack(es[i])
		if !checkListLinks(t, ls, es[:i+1]...) {
			t.Fatalf("failed at %d", i)
		}
	}
}

func TestList_PushFront(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	ls := NewList[*testListNode]()
	for i := 0; i < len(es); i++ {
		ls.PushFront(es[len(es)-i-1])
		if !checkListLinks(t, ls, es[len(es)-i-1:]...) {
			t.Fatalf("failed at %d", i)
		}
	}
}

func TestList_Remove(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	t.Run("single", func(t *testing.T) {
		ls := NewList(es[0])
		ls.Remove(es[0])
		checkListLinks(t, ls)
	})

	t.Run("front", func(t *testing.T) {
		ls := NewList(es...)
		ls.Remove(es[0])
		checkListLinks(t, ls, es[1:]...)
	})
	t.Run("mid", func(t *testing.T) {
		ls := NewList(es...)
		ls.Remove(es[1])
		checkListLinks(t, ls, es[0], es[2])
	})
	t.Run("back", func(t *testing.T) {
		ls := NewList(es...)
		ls.Remove(es[2])
		checkListLinks(t, ls, es[:2]...)
	})
}

func TestList_MoveToFront(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	ls := NewList(es...)
	ls.MoveToFront(es[0])
	checkListLinks(t, ls, es...)
	ls.MoveToFront(es[1])
	checkListLinks(t, ls, es[1], es[0], es[2])
	ls.MoveToFront(es[2])
	checkListLinks(t, ls, es[2], es[1], es[0])
}

func TestList_MoveToBack(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	ls := NewList(es...)
	ls.MoveToBack(es[2])
	checkListLinks(t, ls, es...)
	ls.MoveToBack(es[1])
	checkListLinks(t, ls, es[0], es[2], es[1])
	// ls.MoveToBack(es[0])
	// checkListLinks(t, ls, es[2], es[1], es[0])
}
