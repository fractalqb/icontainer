package icontainer

type ForwardIter[T any] interface {
	IterNext() T
	SetIterNext(n T)
	comparable
}

type BackwardIter[T any] interface {
	IterPrev() T
	SetIterPrev(n T)
	comparable
}

type Iter[T any] interface {
	ForwardIter[T]
	BackwardIter[T]
}
