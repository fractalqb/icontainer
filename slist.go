package icontainer

type SListNode[E ForwardIter[E]] struct{ next E }

func (n *SListNode[E]) IterNext() E     { return n.next }
func (n *SListNode[E]) SetIterNext(m E) { n.next = m }

type SList[E ForwardIter[E]] struct {
	front  E
	length int
}

func NewSList[E ForwardIter[E]](init ...E) *SList[E] {
	res := new(SList[E])
	res.PushFrontAll(init...)
	return res
}

func (ls *SList[E]) Len() int { return ls.length }

func (ls *SList[E]) Front() E { return ls.front }

func (ls *SList[E]) PushFront(e E) {
	e.SetIterNext(ls.front)
	ls.front = e
	ls.length++
}

func (ls *SList[E]) PushFrontAll(es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	ls.length += l
	l--
	for i := 0; i < l; i++ {
		es[i].SetIterNext(es[i+1])
	}
	es[l].SetIterNext(ls.front)
	ls.front = es[0]
}

func (ls *SList[E]) DropFront(n int) {
	if n >= ls.Len() {
		ls.Clear()
	} else {
		var zero E
		ls.length -= n
		for ls.front != zero && n > 0 {
			ls.front = ls.front.IterNext()
			n--
		}
	}
}

func (ls *SList[E]) Clear() {
	var zero E
	ls.front = zero
	ls.length = 0
}

func (ls *SList[E]) Revert() {
	if ls.length == 0 {
		return
	}
	var prev, zero E
	n := ls.front
	for n != zero {
		next := n.IterNext()
		n.SetIterNext(prev)
		prev = n
		n = next
	}
	ls.front = prev
}
