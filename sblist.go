package icontainer

type SBList[E ForwardIter[E]] struct {
	SList[E]
	back E
}

func NewSBList[E ForwardIter[E]](init ...E) *SBList[E] {
	res := &SBList[E]{SList: *NewSList[E]()}
	res.PushFrontAll(init...)
	return res
}

func (ls *SBList[E]) PushFront(e E) {
	if ls.Len() == 0 {
		ls.back = e
	}
	ls.SList.PushFront(e)
}

func (ls *SBList[E]) PushFrontAll(es ...E) {
	if len(es) == 0 {
		return
	}
	if ls.Len() == 0 {
		ls.back = es[len(es)-1]
	}
	ls.SList.PushFrontAll(es...)
}

func (ls *SBList[E]) DropFront(n int) {
	if n >= ls.Len() {
		ls.Clear()
	} else {
		var zero E
		ls.length -= n
		for ls.front != zero && n > 0 {
			ls.front = ls.front.IterNext()
			n--
		}
	}
}

func (ls *SBList[E]) Back() E { return ls.back }

func (ls *SBList[E]) PushBack(e E) {
	if ls.Len() == 0 {
		ls.PushFront(e)
		ls.back = e
	} else {
		ls.back.SetIterNext(e)
		ls.back = e
	}
}

func (ls *SBList[E]) Clear() {
	var zero E
	ls.SList.Clear()
	ls.back = zero
}

func (ls *SBList[E]) Revert() {
	if ls.Len() == 0 {
		return
	}
	tmp := ls.front
	ls.SList.Revert()
	ls.back = tmp
}
