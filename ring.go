package icontainer

type Ring[E Iter[E]] struct {
	hook   E
	length int
}

func NewRing[E Iter[E]](init ...E) *Ring[E] {
	res := new(Ring[E])
	res.PushBackAll(init...)
	return res
}

func (r *Ring[E]) Len() int { return r.length }

func (r *Ring[E]) Hook() E { return r.hook }

func (r *Ring[E]) MoveForward() {
	if r.length > 0 {
		r.hook = r.hook.IterNext()
	}
}

func (r *Ring[E]) MoveBackward() {
	if r.length > 0 {
		r.hook = r.hook.IterPrev()
	}
}

func (r *Ring[E]) Move(n int) {
	if r.length < 2 {
		return
	}
	if n < 0 {
		n = -n % r.length
		for n > 0 {
			r.hook = r.hook.IterPrev()
			n--
		}
	} else {
		n %= r.length
		for n > 0 {
			r.hook = r.hook.IterNext()
			n--
		}
	}
}

func (r *Ring[E]) PushFront(e E) {
	if r.length == 0 {
		e.SetIterNext(e)
		e.SetIterPrev(e)
		r.hook = e
		r.length = 1
	} else {
		r.InsertBefore(e, r.hook)
	}
}

func (r *Ring[E]) PushBack(e E) {
	if r.length == 0 {
		e.SetIterNext(e)
		e.SetIterPrev(e)
		r.hook = e
		r.length = 1
	} else {
		r.InsertAfter(e, r.hook)
	}
}

func (r *Ring[E]) InsertBefore(e, mark E) {
	e.SetIterNext(mark)
	e.SetIterPrev(mark.IterPrev())
	mark.SetIterPrev(e)
	e.IterPrev().SetIterNext(e)
	r.length++
}

func (r *Ring[E]) InsertAfter(e, mark E) {
	e.SetIterNext(mark.IterNext())
	e.SetIterPrev(mark)
	mark.SetIterNext(e)
	e.IterNext().SetIterPrev(e)
	r.length++
}

func (r *Ring[E]) PushFrontAll(es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	if r.length == 0 {
		r.length = l
		l--
		es[0].SetIterPrev(es[l])
		es[l].SetIterNext(es[0])
		r.hook = es[l]
	} else {
		r.length += l
		l--
		ringLink(r.hook.IterPrev(), es[0])
		ringLink(es[l], r.hook)
	}
}

func (r *Ring[E]) InserBeforeAll(mark E, es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	r.length += l
	ringLink(mark.IterPrev(), es[0])
	ringLink(es[l-1], mark)
}

func (r *Ring[E]) PushBackAll(es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	if r.length == 0 {
		r.length = l
		l--
		es[0].SetIterPrev(es[l])
		es[l].SetIterNext(es[0])
		r.hook = es[0]
	} else {
		r.length += l
		l--
		ringLink(r.hook, es[0])
		ringLink(es[l], r.hook.IterNext())
	}
}

func (r *Ring[E]) InserAfterAll(mark E, es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	r.length += l
	ringLink(mark, es[0])
	ringLink(es[l-1], mark.IterNext())
}

func (r *Ring[E]) Remove(e E, hookFwd bool) {
	if r.length == 1 {
		var zero E
		r.hook = zero
		r.length = 0
		return
	}
	if r.hook == e {
		if hookFwd {
			r.hook = r.hook.IterNext()
		} else {
			r.hook = r.hook.IterPrev()
		}
	}
	e.IterPrev().SetIterNext(e.IterNext())
	e.IterNext().SetIterPrev(e.IterPrev())
	r.length--
}

func ringLink[E Iter[E]](n, m E) {
	n.SetIterNext(m)
	m.SetIterPrev(n)
}
