# Typesafe Intrusive Containers for Go

For a detailed explanation one may read about [Boost.intrusive](https://www.boost.org/doc/libs/1_74_0/doc/html/intrusive.html#intrusive.introduction). The following is a TL;DR:

Intrusive containers are less general than “normal” containers but bring some advantages for  
some special situations. I'd say:

1. If the data to be put into the container can only be in at most one container at any time and
2. one can arrange the data to have some container-specific extra attributes

then you may consider to use intrusive containers. This will help to avoid memory
allocations to maintain the container's structure. Think of it as your data elements are lying
around anyway and the container only rearranges their relations.