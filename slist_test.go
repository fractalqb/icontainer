package icontainer

import (
	"fmt"
	"unsafe"
)

type TestNode1 struct {
	p, n *TestNode1
	Txt  string
}

func (d *TestNode1) IterNext() *TestNode1     { return d.n }
func (d *TestNode1) IterPrev() *TestNode1     { return d.p }
func (d *TestNode1) SetIterNext(n *TestNode1) { d.n = n }
func (d *TestNode1) SetIterPrev(p *TestNode1) { d.p = p }

func ExampleSList_TestNode1() {
	ls := NewSList(&TestNode1{Txt: "#1"}, &TestNode1{Txt: "#2"})
	ls.PushFrontAll(&TestNode1{Txt: "#3"})
	ls.PushFront(&TestNode1{Txt: "#4"})
	for n := ls.Front(); n != nil; n = n.IterNext() {
		fmt.Println(n.Txt)
	}
	// Output:
	// #4
	// #3
	// #1
	// #2
}

func ExampleSList_TestNode2() {
	type TestNode2 struct {
		SListNode[*TestNode2]
		Txt string
	}

	ls := NewSList(&TestNode2{Txt: "#1"}, &TestNode2{Txt: "#2"})
	ls.PushFrontAll(&TestNode2{Txt: "#3"})
	ls.PushFront(&TestNode2{Txt: "#4"})
	fmt.Println("Node Size:", unsafe.Sizeof(SListNode[*TestNode1]{}))
	for n := ls.Front(); n != nil; n = n.IterNext() {
		fmt.Println(n.Txt)
	}
	// Output:
	// Node Size: 8
	// #4
	// #3
	// #1
	// #2
}

func ExampleSList_Revert() {
	type node struct {
		SListNode[*node]
		txt string
	}
	ls := NewSList(&node{txt: "baz"}, &node{txt: "bar"}, &node{txt: "foo"})
	ls.Revert()
	for n := ls.Front(); n != nil; n = n.IterNext() {
		fmt.Println(n.txt)
	}
	// Output:
	// foo
	// bar
	// baz
}
