package icontainer

type ListNode[E Iter[E]] struct{ prev, next E }

func (n *ListNode[E]) IterNext() E     { return n.next }
func (n *ListNode[E]) SetIterNext(m E) { n.next = m }

func (n *ListNode[E]) IterPrev() E     { return n.prev }
func (n *ListNode[E]) SetIterPrev(m E) { n.prev = m }

type List[E Iter[E]] struct {
	front, back E
	length      int
}

func NewList[E Iter[E]](init ...E) *List[E] {
	res := new(List[E])
	res.PushBackAll(init...)
	return res
}

func (ls *List[E]) Len() int { return ls.length }

func (ls *List[E]) Front() E { return ls.front }
func (ls *List[E]) Back() E  { return ls.back }

func (ls *List[E]) PushBack(e E) {
	var zero E
	if ls.length == 0 {
		ls.front, ls.back = e, e
		e.SetIterPrev(zero)
		e.SetIterNext(zero)
		ls.length = 1
	} else {
		e.SetIterPrev(ls.back)
		e.SetIterNext(zero)
		ls.back.SetIterNext(e)
		ls.back = e
		ls.length++
	}
}

func (ls *List[E]) PushBackAll(es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	var zero E
	if ls.length == 0 {
		ls.length = l
		l--
		ls.front = es[0]
		ls.back = es[l]
		es[0].SetIterPrev(zero)
		es[l].SetIterNext(zero)
	} else {
		ls.length += l
		ls.back.SetIterNext(es[0])
		es[0].SetIterPrev(ls.back)
		ls.back = es[0]
		es[l-1].SetIterNext(zero)
	}
}

func (ls *List[E]) PushFront(e E) {
	var zero E
	if ls.length == 0 {
		ls.front, ls.back = e, e
		e.SetIterPrev(zero)
		e.SetIterNext(zero)
		ls.length = 1
	} else {
		e.SetIterPrev(zero)
		e.SetIterNext(ls.front)
		ls.front.SetIterPrev(e)
		ls.front = e
		ls.length++
	}
}

func (ls *List[E]) PushFrontAll(es ...E) {
	l := len(es)
	if l == 0 {
		return
	}
	listChain(es)
	var zero E
	if ls.length == 0 {
		ls.length = l
		l--
		ls.front = es[0]
		ls.back = es[l]
		es[0].SetIterPrev(zero)
		es[l].SetIterNext(zero)
	} else {
		ls.length += l
		l--
		ls.front.SetIterPrev(es[l])
		es[l].SetIterNext(ls.front)
		ls.front = es[l]
		es[0].SetIterPrev(zero)
	}
}

func (ls *List[E]) Clear() {
	var zero E
	ls.front, ls.back = zero, zero
	ls.length = 0
}

// e must be in ls! (Cannot be checked)
func (ls *List[E]) Remove(e E) {
	var zero E
	if ls.front == e {
		ls.front = e.IterNext()
	}
	if ls.back == e {
		ls.back = e.IterPrev()
	}
	if p := e.IterPrev(); p != zero {
		p.SetIterNext(e.IterNext())
	}
	if n := e.IterNext(); n != zero {
		n.SetIterPrev(e.IterPrev())
	}
	ls.length--
}

func (ls *List[E]) MoveToFront(e E) {
	if ls.front == e {
		return
	}
	var zero E
	ls.Remove(e)
	ls.length++
	e.SetIterNext(ls.front)
	e.SetIterPrev(zero)
	ls.front.SetIterPrev(e)
	ls.front = e
}

func (ls *List[E]) MoveToBack(e E) {
	if ls.back == e {
		return
	}
	var zero E
	ls.Remove(e)
	ls.length++
	e.SetIterNext(zero)
	e.SetIterPrev(ls.back)
	ls.back.SetIterNext(e)
	ls.back = e
}

func listChain[E Iter[E]](es []E) {
	l := len(es)
	if l < 2 {
		return
	}
	l--
	es[0].SetIterNext(es[1])
	es[l].SetIterPrev(es[l-1])
	for i := 1; i < l; i++ {
		es[i].SetIterPrev(es[i-1])
		es[i].SetIterNext(es[i+1])
	}
}
