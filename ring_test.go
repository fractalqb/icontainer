package icontainer

import (
	"fmt"
	"testing"
)

func ExampleRing() {
	type node struct {
		ListNode[*node]
		Value string
	}
	r := NewRing(
		&node{Value: "foo"},
		&node{Value: "bar"},
		&node{Value: "baz"},
	)
	for n, i := r.Hook(), r.Len(); i > 0; n, i = n.IterNext(), i-1 {
		fmt.Println(n.Value)
	}
	// Output:
	// foo
	// bar
	// baz
}

func TestRing_Move(t *testing.T) {
	es := []*testListNode{{val: "foo"}, {val: "bar"}, {val: "baz"}}
	t.Run("forward", func(t *testing.T) {
		r := NewRing(es...)
		r.Move(2)
		if r.Hook().val != es[2].val {
			t.Errorf("moved to '%s', expect '%s'", r.hook.val, es[2].val)
		}
	})
	t.Run("backward", func(t *testing.T) {
		r := NewRing(es...)
		r.Move(-2)
		if r.Hook().val != es[1].val {
			t.Errorf("moved to '%s', expect '%s'", r.hook.val, es[1].val)
		}
	})
}
